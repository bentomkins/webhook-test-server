const { Webhooks, createNodeMiddleware } = require("@octokit/webhooks");
const { sign } = require("@octokit/webhooks-methods");
const { exec } = require("child_process");
const express = require("express");
const app = express();


// SECRET AS CONFIGURED IN GITHUB WEBHOOK SETTINGS :
const secret = "mysecret";


// SET PORT :
const PORT = process.env.PORT || 3000;


// MIDDLEWARE WRAPPER TO RESOLVE ASYNC INJECTING THE MISSING SHA256 HEADER :
app.useP = (...args) => {
    wrap = fn => (async function(req, res, next) {
        try { await fn(req, res, next) } 
        catch(e) { next(e) }
    });
    let newArgs = args.map(arg => ( typeof arg === "function" ? wrap(arg) : arg ));
    app.use( ...newArgs );
}


// REMOVE EXPRESS DEFAULT HEADER :
app.disable('x-powered-by');


// ADD MIDDLEWARE TO ACCESS THE REQUEST BODY :
app.use(express.json());


// CONDITIONAL GITHUB ENTERPRISE SHA256 HEADER INJECTION :
app.useP(async (req, res, next) => {
    if (!req.headers['x-hub-signature-256']) {
        req.headers['x-hub-signature-256'] = await sign(secret, JSON.stringify(req.body));
    }
    console.log('REQUEST HEADERS:',  JSON.stringify(req.headers));
    next();
});


// INITIALIZE WEBHOOK OBJECT :
const webhooks = new Webhooks({ secret });


// LISTEN FOR SOMEONE STARRING THE REPOSITORY :
webhooks.on("star", async ({id, name, payload}) => {
    console.log('GITHUB EVENT: ', 'star');
    // TEST BASH COMMAND SCENARIO :
    exec("ls -la", (error, stdout, stderr) => {
        if (error) {
            console.log(`ERROR: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`STDERR: ${stderr}`);
            return;
        }
        console.log(`STDOUT: ${stdout}`);
    });
});


// INSTANTIATE THE ABOVE DEFINED WEBHOOKS SERVER AND PATH :
app.use(createNodeMiddleware(webhooks, { path: "/" }));


// CONSOLE HELPER :
app.listen(PORT, () => { console.log(`LISTENING ON PORT ${PORT}`) });
